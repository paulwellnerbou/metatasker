metatasker
==========

# Usage

## Konfiguration

Es gibt einige Konfigurationen, die man vor dem Start beachten muss:

* Projekt-ID und Task-Sub-Typ in der `task.json`.
* Variablen in der Datei `src/metatasker.groovy` im Block markiert mit `CONFIGURATION START/END`
* Metatasks: Die Tasks, die automatisch angelegt werden, sind in der Variable `metaTasks` konfiguriert, in dem Schema:

```
[
   "Task-Name": "Task-Description",
   "Weiterer Task": "",
   ...
] 
```

## Starten

```
groovy src/metatasker.groovy
```