import groovy.json.JsonBuilder
import groovy.json.JsonSlurper
import groovyx.net.http.RESTClient
import org.apache.http.HttpRequest
import org.apache.http.HttpRequestInterceptor
import org.apache.http.protocol.HttpContext

import static groovyx.net.http.ContentType.*
import static groovyx.net.http.Method.*

@Grab(value = 'org.codehaus.groovy:groovy-all:2.4.7', initClass = false)
@Grapes([
        @Grab(group = 'org.codehaus.groovy.modules.http-builder', module = 'http-builder', version = '0.7.1'),
        @GrabExclude('org.codehaus.groovy:groovy')
])

/* CONFIGURATION START */
def jiraApiUrl = 'https://jira.<YOUR TOP LEVEL DOMAIN>'
def sprint = '6.7';
def searchParams = [jql: "Sprint = \"Sprint ${sprint}\" AND type in (\"New Feature\", Story) AND status in (Open, \"In Progress\") AND summary !~ \"Entwicklungsaufwände\"".toString()]
def dryRun = true
def metaTasks = ["DoD": """* Code-Qualität (Sonar, Review)
* Testfälle/Teststrategie
* Tests (Unit, Integration, Web)
* Browserkompatibilität (Firefox AHP und nicht Sysadmin bei GUI)
* Bedienhandbuch/Benutzerhandbuch
* Architekturdokumentation (wenn nötig)
* Schnittstellenbeschreibung (wenn nötig)
* Release TODOs?
* Deployment auf dev02/test01-3 inkl. Release TODOs
* Logfiles sauber, auch bei deplublizierten Inhalten""",
             "Code Review": "",
             "Review-Vorbereitung": "",
             "Abnahme": "",
             "Kreuztest": "",
             "Merge Master/Release->Feature vor Abnahme": "",
             "Merge Feature->Master/Release nach Abnahme": "",
]
/* CONFIGURATION END */

def restApiPath = '/rest/api/2/'
RESTClient jiraClient = new RESTClient(jiraApiUrl+restApiPath);
def serverInfo = jiraClient.get(path: 'serverInfo')

println serverInfo.data

def username = System.console().readLine 'Username: '
def password = System.console().readPassword 'Password: '

def login = "$username:$password";
def basic = login.toString().bytes.encodeBase64().toString()
jiraClient.client.addRequestInterceptor(new HttpRequestInterceptor() {
            void process(HttpRequest httpRequest, HttpContext httpContext) {
                httpRequest.addHeader('Authorization', 'Basic ' + basic)
            }})

println "Fetching issue list..."
def issueList = fetchSprintIssues(jiraClient, searchParams);

issueList.each() {it
    durchnudeln(jiraClient, it, dryRun, metaTasks)
}

def durchnudeln(RESTClient jiraClient, parent, dryRun, metaTasks) {
    metaTasks.each() { key, value ->
        if(checkIfSubTaskExists(jiraClient, parent, key)) {
            println "Subtask `${key}' exists already for ${parent}."
        } else {
            println "Creating subtask `${key}' for ${parent}."
            if(dryRun) {
                println "Dryrun, not doing anything."
            } else {
                def task = readJson(parent, key, value)
                def response = postIssue(jiraClient, task)
                println response.data
            }
        }
    }
}

def readJson(parent, summary, description) {
    def json = new JsonSlurper().parseText(new File('task.json').text)
    json.fields.summary = summary
    json.fields.parent.key = parent
    json.fields.description = description + "\n\n(Dieser Task wurde automatisiert erstellt.)"
    return json
}

def postIssue(RESTClient jiraClient, params) {
    def jsonBuilder = new JsonBuilder()
    def requestBody = jsonBuilder(params)
    println(jsonBuilder.toPrettyString())
    try {
        return jiraClient.post(requestContentType: JSON, path: 'issue/', body: requestBody)
    } catch (ex) {
        return ex.response
    }
}

def fetchSprintIssues(RESTClient jiraClient, searchParams) {
    println searchParams
    def retList = []
    try {
        def response = jiraClient.post(requestContentType: JSON, path: 'search', body: searchParams)
        response.data.issues.each() {
            retList.add(it.key)
        }
        return retList
    } catch (ex) {
        println ex.response
        throw ex
    }
}

def checkIfSubTaskExists(RESTClient jiraClient, parentIssue, subtaskSummary) {
    def searchParams = [maxResults: 0, jql: "summary ~ \"${subtaskSummary}\" AND parent = \"${parentIssue}\"".toString()]
    try {
        // println searchParams
        def response = jiraClient.post(requestContentType: JSON, path: 'search', body: searchParams)
        if(response.data.total > 0) {
            return true
        } else {
            return false
        }
    } catch (ex) {
        println ex.response
        throw ex
    }
}
